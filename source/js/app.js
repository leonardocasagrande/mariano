// var url = document.location.origin + "/novo";
var url = document.location.origin + "/mariano";

console.log(url);

const widthScreen = window.innerWidth;

function removeCarousel() {
  if (widthScreen > 1000) {
    console.log(widthScreen);

    const dicas = document.getElementsByClassName("owl-dicas");

    dicas[0].classList.remove("owl-dicas", "owl-carousel");
  }
}

$(document).ready(function () {
  $(".owl-planos").owlCarousel({
    loop: false,
    margin: 0,
    nav: true,
    autoplay: false,
    autoplayTimeout: 5000,
    autoplayHoverPause: true,
    dotsContainer: "#materiaDots",
    navContainer: "#materiaNav",
    navText: [
      "<i class='fas fa-arrow-left color-active next'></i>",
      "<i class='fas fa-arrow-right ml-3'></i>",
    ],
    responsive: {
      0: {
        items: 1,
      },
      1000: {
        items: 3,
      },
    },
  });

  $(".owl-dicas").owlCarousel({
    loop: false,
    margin: 0,
    nav: true,
    autoplay: false,
    autoplayTimeout: 5000,
    autoplayHoverPause: true,
    navContainer: "#dicasNav",
    dots: false,
    navText: [
      "<i class='fas fa-arrow-left btn-carousel color-active next'></i>",
      "<i class='fas fa-arrow-right btn-carousel ml-3'></i>",
    ],
    responsive: {
      0: {
        items: 1,
      },
      1000: {
        items: 3,
      },
    },
  });

  $(".owl-historia").owlCarousel({
    loop: false,
    margin: 0,
    nav: true,
    autoplay: false,
    autoplayTimeout: 5000,
    autoplayHoverPause: true,
    dotsContainer: "#materiaDots",
    navContainer: "#materiaNav",
    navText: [
      "<i class='fas fa-arrow-left color-active next'></i>",
      "<i class='fas fa-arrow-right ml-3'></i>",
    ],
    responsive: {
      0: {
        items: 1,
      },
      1000: {
        items: 3,
      },
    },
  });
});

// ------------------------------------------------------------------------------------------------

$("#searchButton").click(function (e) {
  e.preventDefault();
  $(".dica-item").remove();
  let inputValue = $("#searchValue").val();

  $.ajax({
    url: `${url}/wp-json/wp/v2/posts?_embed`,
    data: {
      search: inputValue,
    },
    beforeSend: function () {
      $("#searchContainer").empty();
      let loadingIcon = `
      <div class="loading">
        <div class="loader"></div>
      </div>`;

      $("#searchContainer").append(loadingIcon);
    },
  }).done(function (data) {
    let emptyContent = `<div style="height:400px"><p>Não encontramos o que você procura</p></div>`;

    if (data.length == 0) {
      $("#searchContainer").append(emptyContent);
      $("#loadMore").remove();
    }
    // console.log(data.length)

    $(".loading").remove();
    let content = `
        ${data
          .map(
            (element) => ` 
        <a class="dica-item " href="${element.link}">
          <div class="icon">
            <div class="dica-img" style="background-image:url(${element.featured_image_src})">
              <div class="tag-item tag-position">${element.categories_names[0]}</div>
              <p class="item-caption">${element.date}</p>

            </div>
            <h3 class="text-warning pt-3 pb-1">${element.title.rendered}</h3>
            <p class="pb-3">${element.excerpt.rendered}</p>
            <div class="mb-5 detalhe-dica"></div>
          </div>
        </a>`
          )
          .join("")}`;

    $("#searchContainer").append(content);
  });
});

// ------------------------------------------------------------------------------------------------

let pageNumber = 1;
$("#loadMore").click(function (e) {
  e.preventDefault();
  $("#loadMore span").empty();

  var categorie = $("#loadMore").data("categorie");
  var tag = $("#loadMore").data("tag");
  if (categorie == undefined) {
    categorie = "";
  }
  if (tag == undefined) {
    tag = "";
  }
  pageNumber += 1;
  $.ajax({
    url: `${url}/wp-json/wp/v2/posts?per_page=1&page=${pageNumber}&categories=${categorie}&tags=${tag}`,
    data: {},
    statusCode: {
      200: function (data, status, xhr) {
        const numPosts = xhr.getResponseHeader("X-WP-TotalPages");

        let content = `
        ${data
          .map(
            (element) => ` 
        <a class="dica-item " href="${element.link}">
          <div class="icon">
            <div class="dica-img" style="background-image:url(${
              element.featured_image_src
            })">
    
            ${element.categories_names
              .map(
                (content) => `
            <div class="tag-item tag-position">${content}</div>
            `
              )
              .join("")}
              <p class="item-caption">${element.formatted_date}</p>
            </div>
            <h3 class="text-warning pt-3 pb-1">${element.title.rendered}</h3>
            <p class="pb-3">${element.excerpt.rendered}</p>
            <div class="mb-5 detalhe-dica"></div>
          </div>
        </a>`
          )
          .join("")}`;
        if (pageNumber >= numPosts) {
          $("#loadMore").remove();
        }

        $("#searchContainer").append(content);
      },
    },
  });
});

$(".submit-input").submit(function (e) {
  e.preventDefault();
});
