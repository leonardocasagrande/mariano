<!-- FOOTER -->

<section>
    <!-- GET CONTATO -->
    <?php

    if (!is_page('contato')) {
    } elseif (!is_singular('servico')) {
    } else {
        get_template_part('contato');
    }
    ?>
</section>

<div class="marcas py-4 d-none d-md-block">

    <div class="text-center">

        <p class="color-black mb-3">SOMOS ESPECIALISTAS:</p>


        <div class="d-flex align-items-center px-4">
            <div class="col"><img src="<?= get_stylesheet_directory_uri() ?>/dist/img/logos/fiat-logo.png" alt="Logo Fiat"></div>
            <div class="col"><img src="<?= get_stylesheet_directory_uri() ?>/dist/img/logos/ford-logo.png" alt="Logo Ford"></div>
            <div class="col"><img src="<?= get_stylesheet_directory_uri() ?>/dist/img/logos/Chevrolet-logo.png" alt="Logo Chevrolet"></div>
            <div class="col"><img src="<?= get_stylesheet_directory_uri() ?>/dist/img/logos/Volkswagen-Logo.png" alt="Logo Volkswagen"></div>
            <div class="col"><img src="<?= get_stylesheet_directory_uri() ?>/dist/img/logos/renault-logo.png" alt="Logo Renault"></div>
            <div class="col"><img src="<?= get_stylesheet_directory_uri() ?>/dist/img/logos/toyota-logo.png" alt="Logo Toyota"></div>
            <div class="col"><img src="<?= get_stylesheet_directory_uri() ?>/dist/img/logos/hyundai-logo.png" alt="Logo Hyundai"></div>
            <div class="col"><img src="<?= get_stylesheet_directory_uri() ?>/dist/img/logos/citroen-logo.png" alt="Logo Citroen"></div>
            <div class="col"><img src="<?= get_stylesheet_directory_uri() ?>/dist/img/logos/mitsubish-logo.png" alt="Logo Mitsubishi"></div>
            <div class="col"><img src="<?= get_stylesheet_directory_uri() ?>/dist/img/logos/Peugeot-logo.png" alt="Logo Peugeot"></div>
            <div class="col"><img src="<?= get_stylesheet_directory_uri() ?>/dist/img/logos/kia-logo.png" alt="Logo Kia"></div>
            <div class="col"><img src="<?= get_stylesheet_directory_uri() ?>/dist/img/logos/honda-autos-logo.png" alt="Logo Honda"></div>
            <div class="col"><img src="<?= get_stylesheet_directory_uri() ?>/dist/img/logos/nissan-logo.png" alt="Logo Nissan"></div>

        </div>

    </div>
</div>
<div class="bg-footer position-relative">


    <div class="pt-3 container d-md-flex justify-content-around align-items-start text-center text-md-left">

        <div class="pb-3  pr-md-5 flex-md-column text-center text-md-left">


            <div class="p-5 container d-md-flex justify-content-around align-items-start text-center text-md-left">
                <div class="pb-5 pr-md-5">
                    <img class="mb-2" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/Logo-Mariano-branco.png" alt="">
                    <div class="selo d-none d-md-block">
                        <img src="<?= get_stylesheet_directory_uri() ?>/dist/img/ase-logo.png" alt="Certificado ASE">
                    </div>

                </div>

                <div class="pr-md-3">
                    <h3 class="text-warning p-md-0 justify-content-center text-md-left pb-3">Menu</h3>
                    <a class="d-block  mt-4 " href="<?php echo get_site_url() ?>/o-centro">O Centro Automotivo</a>
                    <a class="d-block  mt-2 " href="<?php echo get_site_url() ?>/servicos">Serviços</a>
                    <a class="d-block  mt-2" href="<?php echo get_site_url() ?>/dicas">Dicas do Mariano</a>
                    <a class="d-block  mt-2" href="<?php echo get_site_url() ?>/promocoes">Promoções</a>
                    <a class="d-block  mt-2" href="<?php echo get_site_url() ?>/frotas-e-locadoras">Frotas e locadoras</a>
                    <a class="d-block  mt-2 " href="http://queridocarro.com/Servicos/Usuario/Login" target="_blank">Histórico online</a>
                    <a class="d-block mt-2 " href="<?php echo get_site_url() ?>/contato">Contato</a>
                </div>

                <div class="px-md-3">
                    <h3 class="text-warning pb-2 p-md-0 pt-5">Endereço</h3>
                    <p class=" my-3 text-white">Rua Belo Horizonte, 129 -<br>
                        Residencial João Luiz<br> CEP 13186-493, <br> Hortolândia / SP</p>
                </div>

                <div class="pl-md-3">
                    <h3 class="text-warning p-md-0 pb-2 pt-5">Contatos</h3>
                    <a href="https://api.whatsapp.com/send?phone=551938651041" class=" d-block pt-3 m-0"><i class="fab fa-whatsapp pr-2"></i>19 3865-1041</a>
                    <a href="tel:+551938090722" class=" d-block my-3"><i class="fas fa-phone-alt pr-2"></i>19 3809-0722</a>

                </div>
            </div>


        </div>
    </div>
</div>

<!-- Principal JavaScript do Bootstrap -->
<?php wp_footer(); ?>

<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
<script src="<?= get_stylesheet_directory_uri(); ?>/dist/js/app.js"></script>
<?php
if (is_front_page()) {
    echo "<script>removeCarousel()</script>";
}
?>
<script>
    new WOW().init();
</script>
</body>

</html>