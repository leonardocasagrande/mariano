
  <!-- GET HEADER -->
<?php get_header() ;?>





  <!-- REVERSE, CABEÇALHO MOBILE -->
  <section class="container px-3 pt-5 text-center">
    <div class="d-md-flex flex-md-column-reverse">
      <div class="py-3 pl-md-4 justify-content-md-start row  justify-content-around align-items-center">
        <div class="row w-75 justify-content-between justify-content-md-start align-items-center">


        <? global $post; $categories = get_the_category($post->ID);?>
        <? foreach( $categories as $category ): ?>
          <a class="tag-item mr-md-5" href="#"><?= $category->cat_name; ?></a>
        <? endforeach; ?>

          <span class="text-data"><? the_date();?></span>
        </div>

        <!-- COMPARTILHAR E VOLTAR DESKTOP -->
        <div class="d-md-block d-none w-25 text-md-right">
          <span class="text-dica">Compartilhar</span>
          <div class="pt-3 row justify-content-md-end pr-md-3 justify-content-center">
            <a class="pr-3" href="#"><i class="text-warning fa fa-facebook"></i></a>
            <a class="pr-3" href="#"><i class="text-warning fa fa-twitter"></i></a>
            <a class="pr-3" href="#"><i class="text-warning fa fa-google-plus"></i></a>
            <a class="" href=" #"><i class="text-warning fa fa-linkedin"></i></a>
          </div>
        </div>

        <div class="py-md-3 icon  d-md-flex d-none  ">
          <a href="<?php echo get_site_url(); ?>/dicas">
            <i class="fa fa-angle-left pr-2 text-warning font-weight-bold"></i>
            <span class="text-dica">Voltar</span>
          </a>
        </div>
      </div>

      <!-- IMAGEM -->
      <div class="pb-4">
        <img class="img-fluid responsive-img" src="<?php echo get_the_post_thumbnail_url();?>" alt="carros">
      </div>

      <!-- VOLTAR MOBILE -->
      <div class="px-2 pb-3 icon d-md-none text-left">
        <a href="<?php echo get_site_url(); ?>/dicas">
          <i class="fa fa-angle-left pr-2 text-warning font-weight-bold"></i>
          <span class="text-dica">Voltar</span>
        </a>
      </div>

      <!-- TITULO  -->
      <h3 class="text-warning titulo pb-md-5 pb-4"><?php echo get_the_title();?> </h3>
    </div>


    <div class="dica-conteudo px-2  px-md-5">

    <?php
        if (have_posts()):
          while (have_posts()) : 
            the_post();


            the_content();
            
          endwhile;
        else:
          echo '<p>Sorry, no posts matched your criteria.</p>';
        endif;
      ?>
    </div>

    <!-- COMPARTILHAR MOBILE -->
    <div class="d-md-none pb-5">
      <span class="text-dica">Compartilhar</span>
      <div class="pt-3 icon row justify-content-center">
        <a class="pr-3" href="#"><i class="text-warning fa fa-facebook"></i></a>
        <a class="pr-3" href="#"><i class="text-warning fa fa-twitter"></i></a>
        <a class="pr-3" href="#"><i class="text-warning fa fa-google-plus"></i></a>
        <a class="" href=" #"><i class="text-warning fa fa-linkedin"></i></a>
      </div>
    </div>
    
  </section>
    
  
<?php get_template_part('contato'); ?>

  <!-- GET FOOTER -->
<?php get_footer(); ?>