<?php $agrs = array(
        'post_type' => 'servico',
        'posts_per_page' => -1
    ); 
    $servicos = new WP_Query($agrs);
    $contador = 1;
    ?>
<div class="container d-md-none p-3">
<div class="accordion">
  <button class="accordion-toggle collapsed" type="button" id="accordionMenuButton" data-toggle="collapse" aria-expanded="false" data-target="#collapseOne">
    Selecionar serviço  <i class="fas fa-caret-down color-yellow"></i>
  </button>
  <div id="collapseOne" class="accordion-menu collapse mt-4" aria-labelledby="accordionMenuButton">
      <ol class="">

      
    <?php 
    if($servicos->have_posts()):while($servicos->have_posts()):$servicos->the_post();
    ?>
    <li><a href="<?php the_permalink(); ?>" class="droplink"><?php the_title() ?></a></li>
<?php endwhile; endif; ?>
</ol>
  </div>
</div>
</div>
<div class="container d-none d-md-block">
<div class="row servico-list mt-4">
<?php 
    if($servicos->have_posts()):while($servicos->have_posts()):$servicos->the_post();
    ?>
    <div class="col-md-4 servico-item" style="background-image:url(<?php echo get_the_post_thumbnail_url() ?>)">
    <a href="<?php the_permalink() ?>">
    <div>
    <span class="color-yellow"><?php echo $contador; ?></span>
    <h2><?php the_title() ?></h2>
    </div>
    <hr>
    </a>
    </div>
    <?php $contador++; endwhile; endif; ?>
</div>
</div>