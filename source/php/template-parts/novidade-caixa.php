<div class="novidade-caixa bg-gray">
    <div class="container mb-md-5 position-relative">
        <div class="row flex-wrap-reverse align-items-center justify-content-between">
            <div class="col-md-6 col-lg-4">
                <img class="box-mariano" src="<?= get_stylesheet_directory_uri() ?>/dist/img/box-mariano.png" alt="Caixa de devolução das peças">
            </div>
            <div class=" col-md-6">
                <span class="color-yellow">Novidade</span>
                <h2>Caixa de devolução<br> das peças</h2>
                <p>
                    Para dar mais transparência e confiança aos clientes, nós entregamos uma caixa personalizada com devolução das peças substituídas durante o conserto do carro.
                </p>
            </div>

        </div>
    </div>
    <a href="<?= get_site_url() ?>/contato" class="btn-amarelo">Entrar em contato</a>
</div>