<?php get_header();
the_post(); ?>

<!-- BANNER -->
<div class="bg-dicas">
  <div class="caption px-3">
    <h1 class="pb-3">Dicas do Mariano</h1>
    <p class="px-5 text-white">Dê uma olhada nas dicas que o Mariano tem pra você:</p>
  </div>
</div>


<!-- SEARCH  -->
<section class="container py-md-4 pb-5 dica-item">
  <?php the_content() ?>
  <!-- <aside class="pt-md-5 col-md-4 position-sticky container py-5">
      <div class="search-bar">
        <input class="col-12" type="text" name="Pesquisar" id="searchValue" placeholder="Pesquisar">
        <a id="searchButton" href="#"><i class="fa fa-search"></i></a>
      </div>

      DROPDOWN MOBILE
      <div class="row d-md-none m-0 justify-content-between">
        <div class="dropdown pr-3">
          <button class="  btn-drop dropdown-toggle" type="button" id="dropdownCategoria" data-toggle="dropdown"
            aria-haspopup="true" aria-expanded="false">
            <span>Categorias</span>
          </button>
          <div class="dropdown-menu" aria-labelledby="dropdownCategoria">

          <?php
          $categories = get_categories(array(
            'orderby' => 'name',
            'order'   => 'ASC',
            'hide_empty' => false
          ));
          ?>     

          <?php foreach ($categories as $category) : ?>
          <a class="categoria-item d-block" href="<?php echo esc_attr(get_category_link($category->term_id)); ?>"><?php echo esc_html($category->name); ?></a>
          <?php endforeach; ?>

          </div>
        </div>


        <div class="dropdown">
          <button class=" btn-drop dropdown-toggle" type="button" id="dropdownTags" data-toggle="dropdown"
            aria-haspopup="true" aria-expanded="false">
            <span>Tags</span>
          </button>
          <div class="dropdown-menu dropdown-menu-right " aria-labelledby="dropdownTags">

          <?php
          $tags = get_tags(array(
            'orderby' => 'name',
            'order'   => 'ASC',
            'hide_empty' => false
          ));
          ?>     

            <?php foreach ($tags as $tag) : ?>
            <a class="categoria-item d-block" href="<?php echo esc_attr(get_tag_link($tag->term_id)); ?>"><?php echo esc_html($tag->name); ?></a>
            <?php endforeach; ?>

          </div>
        </div>
      </div>

      CATEGORIAS E TAGS DESKTOP 

      <div class="row d-none mt-md-5 d-md-block m-0 justify-content-between">
        <div class="flex-column row">
          <h3 class="pb-md-3 px-md-3">Categorias</h3>


          <?php
          $categories = get_categories(array(
            'orderby' => 'name',
            'order'   => 'ASC',
            'hide_empty' => false
          ));
          ?>     

          <?php foreach ($categories as $category) : ?>

          <a class="categoria-item " href="<?php echo esc_attr(get_category_link($category->term_id)); ?>"><?php echo esc_html($category->name); ?></a>

          <?php endforeach; ?>

        </div>


        <div class="pt-md-5 px-md-2">
          <h3 class="pb-md-3">Tags</h3>
          <div class="row">
          
            <?php
            $tags = get_tags(array(
              'orderby' => 'name',
              'order'   => 'ASC',
              'hide_empty' => false
            ));
            ?>     

            <?php foreach ($tags as $tag) : ?>
            <a class="tag-item" href="<?php echo esc_attr(get_tag_link($tag->term_id)); ?>"><?php echo esc_html($tag->name); ?></a>
            <?php endforeach; ?>

          </div>
        </div>

      </div>

    </aside> -->


  <!-- SECTION 
    <div class="dicas text-center pt-md-5 col-md-8 px-3">


      <?php
      $args = array(
        'post_type' => 'post',
        'posts_per_page' => -1,
      );

      $post_query = new WP_Query($args);

      if ($post_query->have_posts()) {
        while ($post_query->have_posts()) {
          $post_query->the_post();
      ?>
            
           <? global $post;
            $categories = get_the_category($post->ID); ?> 

          <a class="dica-item" href="<?php echo get_permalink(); ?>">
            <div class="icon">
              <div class="dica-img" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>);">

                <? foreach ($categories as $category) : ?>
                <div class="tag-item tag-position"><?= $category->cat_name; ?></div>
                <? endforeach; ?>
                <p class="item-caption"><? the_date(); ?></p>

              </div>
          

                <h3 class="text-warning pt-3 pb-1"><?php the_title(); ?></h3>
                <p class="pb-3"><?php the_content(); ?></p>
              <div class="mb-5 detalhe-dica"></div>
            </div>
          </a>
        <?php }
      }
        ?>


      <div id="searchContainer"></div>

       -->


  <!-- <div id="btnContainer" class="d-flex justify-content-center">
        <a class="btn-load" id="loadMore" href="#"><span>Carregar mais</span></a>
      </div> 

    </div>-->
</section>


<?php get_template_part('contato'); ?>

<!-- GET FOOTER -->
<?php get_footer(); ?>