<?php get_header() ?>
<div class="banner-servicos ">
    <div class="conteudo caption">
        <h1 class="text-center">Serviços</h1>
        <p class="text-center">Entenda a importância da manutenção e os cuidados que você deve ter com cada item do seu veículo.</p>
    </div>
</div>
<section class="servicos  text-center text-md-left pb-4">
    <div class="container">

        <p class="text-center color-text py-4">
            Sempre prezando pela qualidade, realizamos uma ampla variedade de serviços para o seu carro ter o melhor tratamento e você ficar seguro e tranquilo.
            Todos os nossos serviços são realizados seguindo as principais normas das montadoras.
        </p>
    </div>
    <?php get_template_part('menu', 'servicos'); ?>



    <div class="container pt-md-5">
        <hr class="d-md-none">
        <div class="manutencao text-center my-4">
            <span>NÃO PERCA TEMPO</span>
            <h2 class="color-yellow">Faça sua manutenção preventiva agora!</h2>
            <img class=img-fluid src="<?= get_stylesheet_directory_uri() ?>/dist/img/carros-check.png" alt="Verifique a manutenção do seu carro">
            <div class="d-md-flex justify-content-center">
                <p class="col-8">
                    A manutenção preventiva é um item fundamental para que qualquer trajeto seja realizado com segurança e não deve ficar restrita apenas aos finais de ano ou períodos de férias escolares.
                </p>
            </div>
        </div>
        <div class="motivos py-5 mb-5">
            <div class="row">
                <h2 class="col-lg-3">Porque <br>realizar?</h2>
                <div class="col-md-4 col-lg-3">
                    <h3 class="color-yellow">Ajuda a reduzir acidentes</h3>
                    <p>
                        Detecte preventivamente o desgaste das peças e não corra riscos
                    </p>
                </div>
                <div class="col-md-4 col-lg-3">
                    <h3 class="color-yellow">Diminui o tempo de parada</h3>
                    <p>
                        Veículo com a manutenção em dia não vai te deixar na mão
                    </p>
                </div>
                <div class="col-md-4 col-lg-3">
                    <h3 class="color-yellow">Reduz os custos</h3>
                    <p>
                        O custo de consertar um veículo quebrado é sempre muito maior
                    </p>
                </div>
            </div>
        </div>
    </div>


    <?php get_template_part('novidade', 'caixa') ?>


    <div class="container">
        <div class="bateria">
            <div class="ml-md-5  pl-md-5 row align-items-center ">
                <div class="col-md-6 ">
                    <h2 class="color-yellow">Compre sua bateria com o Mariano!</h2>
                    <p>Melhor qualidade e cobre oferta, pois com a bateria há outros serviços.</p>
                </div>
                <div class="col-md-6 ">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/dist/img/bateria.png" alt="Baterias é no Mariano">
                </div>
            </div>
        </div>
    </div>

    <?php get_template_part('contato') ?>


</section>
<?php get_footer() ?>