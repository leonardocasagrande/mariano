<?php
show_admin_bar(false);

add_theme_support('post-thumbnails');

function replace_core_jquery_version()
{
  wp_deregister_script('jquery');
  // Change the URL if you want to load a local copy of jQuery from your own server.
  wp_register_script('jquery', "https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js", array(), '3.4.1');
}
add_action('wp_enqueue_scripts', 'replace_core_jquery_version');


add_action('rest_api_init', 'add_thumbnail_to_JSON');
function add_thumbnail_to_JSON()
{
  //Add featured image
  register_rest_field(
    'post', // Where to add the field (Here, blog posts. Could be an array)
    'featured_image_src', // Name of new field (You can call this anything)
    array(
      'get_callback'    => 'get_image_src',
      'update_callback' => null,
      'schema'          => null,
    )
  );
}

function get_image_src($object, $field_name, $request)
{
  $feat_img_array = wp_get_attachment_image_src(
    $object['featured_media'], // Image attachment ID
    'full',  // Size.  Ex. "thumbnail", "large", "full", etc..
    true // Whether the image should be treated as an icon.
  );
  return $feat_img_array[0];
}


function wpse_287931_register_categories_names_field()
{

  register_rest_field(
    'post',
    'categories_names',
    array(
      'get_callback'    => 'wpse_287931_get_categories_names',
      'update_callback' => null,
      'schema'          => null,
    )
  );
}

add_action('rest_api_init', 'wpse_287931_register_categories_names_field');

function wpse_287931_get_categories_names($object, $field_name, $request)
{

  $formatted_categories = array();

  $categories = get_the_category($object['id']);

  foreach ($categories as $category) {
    $formatted_categories[] = $category->name;
  }

  return $formatted_categories;
}

/**
 * Add a Formatted Date to the WordPress REST API JSON Post Object
 *
 * https://adambalee.com/?p=1547
 */
add_action('rest_api_init', function () {
  register_rest_field(
    array('post'),
    'formatted_date',
    array(
      'get_callback'    => function () {
        return get_the_date();
      },
      'update_callback' => null,
      'schema'          => null,
    )
  );
});

// Our custom post type function
function create_posttype()
{

  register_post_type(
    'promocao',
    // CPT Options
    array(
      'labels' => array(
        'name' => __('Promoções'),
        'singular_name' => __('Promoção')
      ),
      'public' => true,
      'supports' => array('title', 'editor', 'thumbnail'),
      'has_archive' => true,
      'rewrite' => array('slug' => 'promocao'),
      'show_in_rest' => true,

    )
  );
}
// Hooking up our function to theme setup
add_action('init', 'create_posttype');
?>